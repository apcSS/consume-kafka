package com.demo.consumer.repositories;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.demo.consumer.model.Customer;

public interface CustomerRepository extends MongoRepository<Customer,String> {

}
