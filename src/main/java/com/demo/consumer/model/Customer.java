package com.demo.consumer.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.Data;

@Document
@Data
public class Customer {
	@Id
	private Long msgId;
	private String sender;
	private String msg;
	public Customer(Long msgId, String sender, String msg) {
		super();
		this.msgId = msgId;
		this.sender = sender;
		this.msg = msg;
	}
	
}
