package com.demo.consumer.kafka;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.support.KafkaHeaders;
import org.springframework.messaging.handler.annotation.Header;
import org.springframework.stereotype.Component;

import com.demo.consumer.model.Customer;
import com.demo.consumer.services.CustomersService;
import com.example.demo.MsgRequest;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class MessageConsumer {
	private static final String MESSAGE_TOPIC_CUSTOMER = "customer.topic";

	@Autowired
	CustomersService customersService;

	@KafkaListener(topics = MESSAGE_TOPIC_CUSTOMER , groupId = "test-consumer-group")
	public void listen(ConsumerRecord<String, MsgRequest> cr ,@Header(KafkaHeaders.RECEIVED_PARTITION_ID) int partition , @Header(KafkaHeaders.RECEIVED_TIMESTAMP) long ts) {
		log.info("# partition : [ {} ], {} , {} , {} " , partition ,cr.value().getSender().concat("++") , cr.value().getMsg() , new SimpleDateFormat("dd/mm/yyyy HH:mm:ss").format(new Date(ts)));
		customersService.createUsers(new Customer(cr.value().getMsgId(), cr.value().getSender(), cr.value().getMsg()));
	}
}
