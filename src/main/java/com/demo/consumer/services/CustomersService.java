package com.demo.consumer.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.demo.consumer.model.Customer;
import com.demo.consumer.repositories.CustomerRepository;

@Service
public class CustomersService {
	private CustomerRepository repository;
	@Autowired
	public CustomersService(CustomerRepository repository) {
		this.repository = repository;
	}

	public Customer createUsers(Customer c) {
		return repository.save(c);
	}

	public List<Customer> retrieveCustomers() {
		return repository.findAll();
	}

	public Optional<Customer> retrieveCustomers(String id) {
		return repository.findById(id);
	}
}
